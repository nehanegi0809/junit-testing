import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CalculatorTest {

	@Test
	public void testAdd() {
		Calculator calculator = new Calculator();
		int a = 222;
		int b = 333;
		int actual = calculator.add(a, b);

		int expected = 555;
		assertEquals(expected, actual);

	}

	@Test
	public void testSubstracr() {
		Calculator calculator = new Calculator();
		int c = 444;
		int d = 333;
		int actual = calculator.substract(c, d);

		int expected = 111;
		assertEquals(expected, actual);
	}

}
